import app from './App';
import mapboxgl, {LngLatBoundsLike, Map, MapboxOptions, Marker, Popup} from 'mapbox-gl';
import jQuery from 'jquery';
import IEvent from './IEvent';
import IPosition from './IPosition';
const $: JQueryStatic = jQuery;
// <div id="modal" class="modal">
//   <div class="modal-content">
//      <div class="commands">
//          <div class="btn edit">✏️</div>
//          <div class="btn close">❌</div>
//      </div>
//      <div class="title">🥔TITRE</div>
//      <div class="content">BLABLA</div>
//      <div class="date">29/01/2020</div>
//   </div>
// </div>

export default class Event {

    public title: string;
    public content: string;
    public dateStart: Date;
    public dateEnd: Date;
    public domModal: any;
    public domTitle: any;
    public domContent: any;
    public domDateStart: any;
    public domDateEnd: any;
    public marker: Marker;
    public position: IPosition;


    constructor(jsonEvent: any, marker: Marker) {
        //Si on récupère un json vide on crée l'evenement, sinon on peut le modifier ?
        this.marker = marker;
        if(jsonEvent == null)
        {
            this.domModal = document.createElement('div');

            this.domTitle = document.createElement('input');
            this.domTitle.setAttribute('type','text');
            this.domTitle.placeholder = "Veuillez rentrer le titre de votre événement";

            this.domContent = document.createElement('textarea');
            this.domContent.placeholder = "Veuillez rentrer une description pour votre événement";

            this.domDateStart = document.createElement('input');
            this.domDateStart.setAttribute('type','datetime-local');

            this.domDateEnd = document.createElement('input');
            this.domDateEnd.setAttribute('type','datetime-local');

            this.createEvent();
        }
        else {
            this.domModal = document.createElement('div');
            this.domTitle = document.createElement('div');
            this.domContent = document.createElement('pre');

            this.title = jsonEvent.title;
            this.content = jsonEvent.content;
            this.dateStart = new Date(jsonEvent.dateStart);
            this.dateEnd = new Date(jsonEvent.dateEnd);
        }
    }

    createEvent(): void {
        // <div class="modal">
        this.domModal.classList.add('modal');

        // <div class="modal-wrapper">
        const domModalWrapper = document.createElement('div');
        domModalWrapper.classList.add('modal-wrapper');

        // <div class="modal-content">
        const domModalContent = document.createElement('div');
        domModalContent.classList.add('modal-content');
        this.domModal.appendChild(domModalWrapper);

        //<div class="title">
        this.domTitle.classList.add('title');
        domModalContent.appendChild(this.domTitle)

        //<div class="content">
        this.domContent.classList.add('content');
        domModalContent.appendChild(this.domContent);

        //label date-start
        const labelStart = document.createElement('LABEL');
        const titleStart = document.createTextNode("Début de l'événement: ");
        labelStart.setAttribute("for", "date-start");
        labelStart.appendChild(titleStart);
        domModalContent.appendChild(labelStart);

        //<div id="date-start" class="date">
        this.domDateStart.classList.add('date');
        this.domDateStart.setAttribute('id','date-start');
        domModalContent.appendChild(this.domDateStart);

        //label date-end
        const labelEnd = document.createElement('LABEL');
        const titleEnd = document.createTextNode("Fin de l'événement: ");
        labelEnd.setAttribute("for", "date-end");
        labelEnd.appendChild(titleEnd);
        domModalContent.appendChild(labelEnd);

        //<div id="date-end" class="date">
        this.domDateEnd.classList.add('date');
        this.domDateEnd.setAttribute('id','date-end');
        domModalContent.appendChild(this.domDateEnd);


        //<div class="validate">
        const domEventValidate = document.createElement('div');
        domEventValidate.classList.add('validate','btn');
        domEventValidate.textContent = 'Valider';
        domEventValidate.addEventListener('click', this.saveEvent.bind(this))
        domModalContent.appendChild(domEventValidate);

        //<div class="close">
        const domEventClose = document.createElement('div');
        domEventClose.classList.add('close','btn');
        domEventClose.textContent = 'Annuler';
        domEventClose.addEventListener('click', this.removeEvent.bind(this))
        domModalContent.appendChild(domEventClose);

        domModalWrapper.appendChild(domModalContent);

        $('body').append(this.domModal);
    }

    removeEvent(): void {
        this.domModal.remove();
        this.marker.remove();
    }

    saveEvent(): void {
        console.log(this.marker);
        const event: IEvent = {
            title: this.domTitle.value.trim(),
            description: this.domContent.value.trim(),
            position:  this.marker.getLngLat(),
            dateStart: this.domDateStart.value,
            dateEnd: this.domDateEnd.value
        }

        app.addPopup(event,this.marker);

        this.domModal.remove();
    }


}