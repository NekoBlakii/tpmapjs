import { Marker } from "mapbox-gl";

export default interface IMarker {
    green: Array<Marker>,
    orange: Array<Marker>,
    red: Array<Marker>
}
