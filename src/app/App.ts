import config from '../../app.config.json';

import jQuery from 'jquery';
import mapboxgl, {LngLatBoundsLike, Map, MapboxOptions, Marker, Popup} from 'mapbox-gl';

import IPosition from './IPosition';
import Event from './Event';
import IEvent from './IEvent.js';
import moment from 'moment';

import IMarker from './IMarker.js';

const storageKey = "EVENTS";

const $: JQueryStatic = jQuery;
const MapboxGeocoder: any = require( '@mapbox/mapbox-gl-geocoder' );

class App {
    public map: Map;
    public $loader: JQuery;

    public $addBtn: JQuery;
    public $filterBtn: JQuery;
    public $filterWrapper: JQuery;
    public $pickLocation: JQuery;
    public $btnPanel: JQuery;
    public $marker: JQuery;
    public markers: IMarker;

    public events: Array<IEvent>;
    public current: Array<IEvent>;

    public addPosition: boolean;

    constructor() {
        this.$loader = $('#loader');
        this.$btnPanel = $('.btn-panel');
        this.$addBtn = $('#add-btn');
        this.$filterBtn = $('#filter-btn');
        this.$filterWrapper = $('.filter-wrapper');
        this.$pickLocation = $('#footer');

        this.markers = {
            orange: [],
            red: [],
            green: []
        }

        this.events = [];
        this.current =  [];

        this.$pickLocation.hide();
        this.$filterWrapper.hide();
        this.addListeners();
    }

    start(): void {
        this.initMap();
    }

    addListeners(): void {
        // Evenement sur le bouton Filtrer pour afficher ou non les choix
        this.$filterBtn.on('click', function() {
            $('.filter-wrapper').toggle();
        });

        // Evenement pour lancer l'action de localisation de l'evenement
        this.$addBtn.on('click', this.getNewLocalisation.bind(this));

        // Evenement si on clique sur une des checkbox des filtres
        $('.checkbox').change((event) => this.filter(event.target));

    }

    initMap(): void {
        const mapOptions: MapboxOptions = {
            container: 'map',
            style: 'mapbox://styles/mapbox/outdoors-v11',
            minZoom: 2,
            zoom: 2,
            bounds: config.api.mapboxgl.mapBounds as LngLatBoundsLike
        };

        mapboxgl.accessToken = config.api.mapboxgl.accessToken;
        this.map = new mapboxgl.Map( mapOptions );
        this.map.on( 'load', this.onMapLoad.bind( this ) );
    }

    getNewLocalisation(): void {
        this.addPosition = true;
        this.$pickLocation.show();
        this.$pickLocation.html(
            'Veuillez sélectionner une position pour votre événement.'
        )

        this.map.on( 'click', this.onMapClick.bind(this));

    }

    private onMapClick(data: any): void {
        if(this.addPosition)
        {
            this.$btnPanel.hide();
            this.newMarker(data.lngLat);
        }
    }

    newMarker(position: IPosition): void {
        this.$marker = $( '<div class="map-marker default"></div>' );

        let marker: Marker = new mapboxgl.Marker({
            element: this.$marker.get(0)
        });

        marker
            .setLngLat(position)
            .addTo(this.map);

        //On a ajouté le marqueur on bloque le fait d'en faire un nouveau
        this.addPosition = false;
        this.$pickLocation.hide();

        new Event(null,marker);
        this.$btnPanel.show();
    }

    addPopup(event: IEvent,marker: Marker): void {
        this.checkColor(event);

        console.log(event)

        if(event.color == "green")
        {
            this.markers.green.push(marker);
        }
        if(event.color == "orange")
        {
            this.markers.orange.push(marker);
        }
        if(event.color == "red")
        {
            this.markers.red.push(marker);
        }

        // On formate la datetime-local dans notre timezone
        let dateStartFormat = moment(event.dateStart).format("DD/MM/YYYY à h:mm");
        let dateEndFormat = moment(event.dateEnd).format("DD/MM/YYYY à h:mm");

        const popup: Popup = new mapboxgl.Popup({
            className: 'map-popup',
            offset: 22
        });

        popup.setHTML(`
                        <div class="popup-inner">
                            <h4>${event.title}</h4>
                            <div>${event.description}</div>
                            <div>Commence le : ${dateStartFormat}</div>
                            <div>Termine le : ${dateEndFormat}</div>
                            <div>${event.information}</div>
                        </div>
                    `);

        marker.setPopup(popup);
    }

    //Si il y a des données dans le localStorage on vérifie les couleurs/dates et on les affiche
    renderEvents(): void {

        const storage: string | null = localStorage.getItem(storageKey);

        if(storage !=null)
        {
            let data = JSON.parse(storage);
            for(let jsonEvent of data) {
                console.log(jsonEvent);
                this.events.push(jsonEvent);
                this.renderEvent(jsonEvent);
            }
        }

        this.$loader.fadeOut();
    }

    renderEvent(event: IEvent): void {
        // On formate la datetime-local dans notre timezone
        let dateStartFormat = moment(event.dateStart).format("DD/MM/YYYY à h:mm");
        let dateEndFormat = moment(event.dateEnd).format("DD/MM/YYYY à h:mm");

        this.$marker = $( `<div class="map-marker ${event.color}" title="${event.title} - Commence le : ${dateStartFormat} - Fini le : ${dateEndFormat}"></div>` );

        let marker: Marker = new mapboxgl.Marker({
            element: this.$marker.get(0)
        });


        marker
            .setLngLat(event.position)
            .addTo(this.map);

        console.log(event);

        this.addPopup(event,marker);
    }

    // On check les dates de l'événement afin de changer la couleur du marker
    checkColor(event: IEvent): void {
        let today = new Date();

        let dateEventStart = new Date(event.dateStart);
        let dateEventEnd = new Date(event.dateEnd);

        if(today.getTime() > dateEventEnd.getTime())
        {
            // DATE DEPASSEE ROUGE
            this.$marker.addClass('red');
            event.color = 'red';
            event.information = "Quel dommage ! Vous avez raté cet événement";
        }
        else {
            // COMBIEN DE JOURS AVANT LEVENEMENT
            let days = moment(dateEventStart).diff(today,'days');
            if(days > 3)
            {
                // +3JOURS
                this.$marker.addClass('green');
                event.color = 'green';
                event.information = '';
            }
            else {
                // -3JOURS
                this.$marker.addClass('orange');
                event.color = 'orange';
                let hoursLeft = moment(dateEventEnd).diff(today,'hours');
                let timeEnd = moment(moment(dateEventEnd).diff(today)).format('D[ jour(s)] H[ heure(s)]');
                let timeStart = moment(moment(dateEventStart).diff(today)).format('D[ jour(s)] H[ heure(s)]');
                if(days <= 0)
                {
                    // CA VEUT DIRE QUON EST DURANT LEVENEMENT
                    let daysLeft = moment(dateEventEnd).diff(today,'days');
                    if(daysLeft == 0)
                    {
                        event.information = `Attention, l'événement fini dans ${hoursLeft} !`;
                    }
                    else {
                        event.information = `Attention, l'événement fini dans ${timeEnd} !`;
                    }
                }
                else {
                    event.information = `Attention, l'événement commence dans ${timeStart}!`;
                }
            }
        }
        this.current.push(event);
        this.save();
    }

    filter(checkbox: any): void {
        // Ici on récupère sur quelle checkbox on a cliqué et on peut afficher ou cacher les events en fonction de la couleur
        if(checkbox.id == "orange")
        {
            // On boucle sur les marqueurs orange sur la map
            for(let marker of this.markers.orange)
            {
                // si la checkbox est checkée on les ajoute sur la map sinon on les enlève de la map
                if(checkbox.checked)
                {
                    marker.addTo(this.map);
                }
                else {
                    marker.remove();
                }
            }
        }

        if(checkbox.id == "red")
        {
            for(let marker of this.markers.red)
            {
                if(checkbox.checked)
                {
                    marker.addTo(this.map);
                }
                else {
                    marker.remove();
                }
            }
        }

        if(checkbox.id == "green")
        {
            for(let marker of this.markers.green)
            {
                if(checkbox.checked)
                {
                    marker.addTo(this.map);
                }
                else {
                    marker.remove();
                }
            }
        }
    }

    onMapLoad(): void {
        this.map.resize();
        this.renderEvents();
    }

    save() {
        localStorage.setItem(storageKey, JSON.stringify(this.current));
    }
}

const app = new App;

export default app;