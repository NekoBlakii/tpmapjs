import IPosition from "./IPosition";
import { Marker } from "mapbox-gl";

export default interface IEvent {
    title: string;
    description: string;
    information?: string; //Peut etre nul en fonction de la date à venir
    color?: string;
    position: IPosition;
    dateStart: Date;
    dateEnd: Date;
}