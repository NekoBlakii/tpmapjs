export default interface IPosition {
    lng: number;
    lat: number;
}